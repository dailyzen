import os, sys, re, string, logging, time
import random, anydbm, smtplib

ALL_STORIES = set(range(1, 101+1))
WWW_URL = "http://nearfar.org/dailyzen/"
UNSUBSCRIBE_URL = WWW_URL + "do.cgi/unsubscribe/%s,%s"
VERIFICATION_URL = WWW_URL + "do.cgi/verify/%s,%s"
DBFILE = "/home/protected/data/dailyzen.db"

# TODO: unused (From: is same as To:). do something about it.
FROM_ADDR = "mail@nearfar.org"

# Decorator: Call the function once, and return the same return value always.
def call_once(func):
    ret = func()
    return lambda: ret

## Logging, error handling bits

# FIXME: is call_once necessary?
@call_once
def logging_handler():
    "Return the desired and default logging handler"
    hdlr = logging.FileHandler("/home/protected/logs/dailyzen.log")
    formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s")
    hdlr.setFormatter(formatter)
    return hdlr
    
dz = logging.getLogger("dailyzen")
dz.addHandler(logging_handler())
dz.setLevel(logging.DEBUG)

class UserError(Exception):
    pass


## Register/Validate mechanism

def register_user(email):
    """Just register the user giving him (via email) an unique url
    to valid himself against the given email address.
    
    On clicking the URL, `validate_user' function would be called."""
    EMAIL_MSG = r"""Hey there,

Just to make sure that it is really you, please click on this link.
%s

You will then be receiving your daily Zen story shortly.

PS: Not initiated by you? simply ignore this email.

"""
    db = anydbm.open(DBFILE, "c")
    if email in db.keys():
        raise UserError, "email address already found"
    user = dict(email=email, id=random_string(), sent_stories=[], valid=False)
    db[email] = repr(user)
    db.close()
    # send email
    verification_url = VERIFICATION_URL % (email, user["id"])
    send_email(email, [email], "DailyZen Verification", EMAIL_MSG % verification_url)
    dz.info("Verification code sent to %s", email)

def validate_user(email, id):
    db = anydbm.open(DBFILE)
    if email in db.keys():
        user = eval(db[email])
        db.close()
        if user['id'] == id and user['valid'] is False:
            # user validated!
            user['valid'] = True
            write_user(user)
            dz.info("Validated %s", email)
        else:
            raise UserError, "Invalid request"

    else:
        db.close()
        raise UserError, "Email address (%s) not found" % email

def delete_user(email, id):
    db = anydbm.open(DBFILE, "w")
    if email in db.keys():
        user = eval(db[email])
        if user["id"] == id:
            del db[email]
        else:
            raise UserError, "Invalid request"
    else:
        raise UserError, "Email address not found (Perhaps already unsubscribed?)"

def random_string(length=15):
    char_domain = string.ascii_letters + string.digits
    id = []
    for x in range(length):
        id.append(random.choice(char_domain))
    return''.join(id)
              

## Users management

def read_users():
    db = anydbm.open(DBFILE, "c")
    users = []
    for k in db.keys():
        user = eval(db[k])
        if user['valid']:
            users.append(user)
    db.close()
    return users

def write_user(user):
    db = anydbm.open(DBFILE, "w")
    db[user['email']] = repr(user)
    db.close()

    
def get_story(number):
    f = open("101zenstories/%d.html.txt" % number)
    title = f.readline().strip()
    f.readline() # empty line
    story = f.read()
    return title, story

def send_email(from_, toaddrs, subject, body):
    SENDMAIL = "sendmail" # sendmail location
    p = os.popen("%s -t" % SENDMAIL, "w")
    p.write("From: %s\n" % from_)
    p.write("To: %s\n" % ",".join(toaddrs))
    p.write("Subject: %s\n" % subject)
    p.write("\n")
    p.write(body)
    sts = p.close()
    if sts is not None and sts != 0:
        dz.error("sendmail failed with exit status: %d", sts)

# not used: fails in nfshost
def send_email_SMTP(from_, toaddrs, subject, body):
    msg = "From: %s\nSubject: %s\n\n%s" % (from_, subject, body)
    s = smtplib.SMTP('127.0.0.1')
    s.sendmail(from_, toaddrs, msg)


def send_random_story_to_user(user):
    sent_stories = set(user['sent_stories'])
    unsent_stories = ALL_STORIES.difference(sent_stories)
    if len(unsent_stories) == 0:
        # nothing left for this user
        # TODO: should we remove him from db?
        return

    if len(sent_stories) == 0:
        # send his first story
        notice_msg = "Welcome. Here is your first story. From now on, you would get a random story that you were not sent before.\n---\n\n"
    else:
        notice_msg = ""
    
    next = random.choice(list(unsent_stories))
    user["sent_stories"].append(next)
    title, story = get_story(next)
    email = user["email"]

    unsubscribe_url = UNSUBSCRIBE_URL % (user["email"], user["id"])
    signature = "\n\n----\n%s\n\nTo unsubsubcribe,\n%s" % (WWW_URL, unsubscribe_url)
    send_email(email, [email], title, notice_msg + story + signature)
    dz.debug("email: To=%s %s (%d)", email, title, next)
    write_user(user)

def now():
    return long(time.time())

def secs(n): return n
def mins(n): return 60*secs(n)
def hrs(n):  return 60*mins(n)
def days(n): return 24*hrs(n)

def dispatch_all():
    dz.info("Broadcast operation started")
    for user in read_users():
        send_random_story_to_user(user)


## TEST
if __name__ == '__main__':
    print 'TEST'
    if not read_users():
        # add sample users
        write_user(dict(email="srid@nearfar.org", id="12345", sent_stories=[1,6,23], valid=True))
        # write_user(dict(email="foo@example.com", id="84854", sent_stories=[51,78,99, 2, 3]))
    dispatch_all()


