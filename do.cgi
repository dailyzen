#!/usr/bin/env python2.4
# -*- mode: python -*-

import os, logging
import cgi, cgitb
cgitb.enable()

## cgi header
print "Content-type: text/html"
print

from dispatch_email import *

cgilog = logging.getLogger("cgi")
cgilog.addHandler(logging_handler())
cgilog.setLevel(logging.DEBUG)

PATH_INFO = os.environ.get("PATH_INFO", "")
cgilog.debug("Request %s", PATH_INFO)

def validate_email(email):
    """validate email address
    - http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/65215
    """
    EMAIL_RE = "^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$"
    if not(len(email) > 7 and re.match(EMAIL_RE, email)):
        raise UserError, "Not a valid email address"

def page_done(msg):
    print "<title>dailyzen</title>"
    print '<link rel="stylesheet" type="text/css" href="http://nearfar.org/paste.css" />'
    print "<body>"
    print "<h3 style='text-align: center'>%s</h3>" % msg
    print "<p style='text-align: center'>Return to <a href='%s'>dailyzen</></p>" % WWW_URL

try:
    if PATH_INFO == "/subscribe":
        form = cgi.FieldStorage()
	try:
            email = form["email"].value
        except KeyError:
            email = ""
        email = email.strip()

        # silly clickers
        if email.endswith("@example.com"):
            raise UserError, "I need YOUR email"

        validate_email(email)
        register_user(email)
        page_done("To verify your email address, please follow the link sent to you by email.")

    elif PATH_INFO.startswith("/verify"):
        args = PATH_INFO[ len("/verify/") : ]
        email, id = args.split(",")
        validate_user(email, id)
        page_done("You are now subscribed. Welcome. :)")

    elif PATH_INFO.startswith("/unsubscribe"):
        args = PATH_INFO[ len("/unsubscribe/") : ]
        email, id = args.split(",")
        delete_user(email, id)
        page_done("You are unsubscribed. See ya again.")

    elif PATH_INFO == "/send_to_all":
        user_agent = os.environ.get("HTTP_USER_AGENT")
        cgilog.info("/send_to_all requested by agent: [%s]", user_agent)
        if user_agent.startswith("WEBCRON"):
            dispatch_all()
            print "Ok"
        else:
            cgilog.warn("/send_to_all NOT requested by webcron, but [%s]", user_agent)
            print "Pass"
        
    else:
        raise UserError, "Invalid request: %s" % PATH_INFO

except UserError, e:
    cgilog.error("UserError: %s", e)
    page_done("Error: %s" % e)
